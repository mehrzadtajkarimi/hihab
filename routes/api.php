<?php

use App\Http\Controllers\ActiveCodeController;
use App\Http\Controllers\Api\v1\PackageController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\v1\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::prefix('v1')->namespace('Api/v1')->group(function () {

//     return request()->all('Token');
// });

// Route::post('/', function () {
//     return 'hkbbkhybh';
// });


Route::prefix('v1')->namespace('api/v1')->group(function () {
    // auth()->loginUsingId(1);

    Route::post('/', function (Request $request) {
        $UserController = new UserController();
        $ActiveCodeController = new ActiveCodeController();
        $PackageController = new PackageController();


        // if ($request->input('_type') == 'login') {
        //     return $UserController->login($request);
        // }

        // if ($request->input('_type') == 'register') {
        //     return $UserController->register($request);
        // }


        // if ($request->input('_type') == 'otp') {
        //     return $ActiveCodeController->getOtp($request);
        // }
        // return 'test';


        if ($request->input('_type') == 'otp') {
            return $UserController->otp_handler($request);
        }
        if ($request->input('_type') == 'auth') {
            return $UserController->register_user($request);
        }
        if ($request->input('_type') == 'packages') {
            return $PackageController->packages($request);
        }

    });
});
