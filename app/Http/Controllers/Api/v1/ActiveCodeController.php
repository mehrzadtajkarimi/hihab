<?php

namespace App\Http\Controllers;

use App\Model\Activecode;
use Illuminate\Http\Request;

class ActiveCodeController extends Controller
{
    public function getOtp($request)
    {
        // $request->user()->activecode()->where()

        $request->validate([
            'otp' => 'required',
        ]);

        $status = Activecode::verbifyCode($request->otp, $request->user());
        if ($status) {
            $request->user()->activecode()->delete();
            $request->user()->updateOrCreate([
                'mobile_on' => $request->mobile_on
            ]);
        }
        return "کد شما تایید و شماره جدید شما ثبت شد";
    }
}
