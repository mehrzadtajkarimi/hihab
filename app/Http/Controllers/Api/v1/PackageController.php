<?php

namespace App\Http\Controllers\Api\v1;

use App\Model\User;
use App\Model\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\UserAccess;

class PackageController extends Controller
{
    use UserAccess;
    public function packages(Request $request)
    {
        if ($this->UserAccess($request)) {
            dd('توکن و سشن بررسی شده می توانید اطلاعات پکیج را دریافت کنید');
            $packages = Package::get();
            return $packages;
        }
    }
}
