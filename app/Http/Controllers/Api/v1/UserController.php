<?php

namespace App\Http\Controllers\Api\v1;



use DateInterval;
use App\Model\User;
use App\Model\Token;
use App\Model\Session;
use App\Model\Activecode;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Traits\UserAccess;
use App\Notifications\ActiveCodeNotifications;

class UserController extends Controller
{
    use UserAccess;

    public function login(Request $request)
    {
        //TODO session check
        if ($this->checkSessionValid($request)) {
            //TODO login user>

            return "شما قبلا ثبت نام شده اید";
        }
        if ($this->checkActiveCodeValid($request)) {
            return 'کد وارد شده معتبر است';
        }
        return 'ابتدا مراحل ثبت نام را انجام دهید';

        // auth()->loginUsingId(1);
        // if (!auth()->attempt($validData)) {
        //     return 'false';
        // }
        // return 'done';
    }

    public function register(Request $request)
    {
        $request->validate([
            'mobile' => 'required', 'regex:/09(1[0-9]|3[1-9]|2[1-9])-?[0-9]{3}-?[0-9]{4}/',
            //  'unique:users,mobil_on',
            'imei' => 'required', 'digits:15',
            //  'unique:sessions,imi',
            'ip' => 'required', 'ip'
        ]);

        $token1_imi = md5($request->imei);
        $token1_Mobil = md5(substr($request->mobile, 4, 2) + 13);
        $token1_token2 = md5($request->token2);
        $user = User::create([
            'mobil_on' => $request->mobile,
        ]);
        Session::create([
            'imi' => md5($token1_imi . $token1_Mobil . $token1_token2),
            'user_id' => $user->id
        ]);

        // Token::create([
        //     'token' => $request->token2,
        // ]);


        $code = Activecode::generateCode($user, $request);
        // $request->user()->notify(new ActiveCodeNotifications($code, $request->mobil_on));
        return 'کد ارسال شده به موبایل خود را وارد نمایید';
    }


    private function checkSessionValid($request)
    {
        return !!$request->user()->sessions()->where('imi', $request->session)->first();
        //  $dd=$request->user()->sessions()->where('imi', $request->session)->first();
        //  dd($dd);
    }

    public function checkActiveCodeValid($request)
    {
        return !!$request->user()->activecodes()->where('code', $request->otp)->first();
    }
    // protected function postManagerAuth(Request $request)
    // {
    // }






    //add 12 tir day: mr zamani

    public function otp_handler(Request $request)
    {
        $request->validate([
            'mobile' => 'required', 'regex:/09(1[0-9]|3[1-9]|2[1-9])-?[0-9]{3}-?[0-9]{4}/',
            //  'unique:users,mobil_on',
            'imei' => 'required', 'digits:15',
            //  'unique:sessions,imi',
        ]);

        $otp = $this->generate_otp($request->mobile);
        $time = now();
        $time->add(new DateInterval('PT' . 2 . 'M'));

        if ($this->getAliveCode($request->mobile)) {
            $otp = Activecode::where('mobile', $request->mobile)->pluck('code')->last();
            return $this->send_msg($request->mobile, $otp);
        }

        Activecode::create([
            'code' => $otp,
            'mobile' => $request->mobile,
            'expired_at' => $time
        ]);
        if ($this->send_msg($request->mobile, $otp)) {
            session()->put("otp", $otp);
            session()->put("imei", $request->imei);

            return 'کد احراز هویت' . " " . $otp . " " . "ارسال شد";
        }
    }




    public function send_msg($mobile, $msg)
    {
        if ($mobile) {
            return 'کد احراز هویت' . " " . $msg . " " . 'ارسال شد';
        }
        return true;
    }




    public function generate_otp($mobile)
    {
        $otp = substr($mobile, 6, 1);
        $otp += substr($mobile, 9, 1);
        $otp = (int) $otp;
        $otp += 13;
        $otp %= 100;
        $otp = rand(1, 9) . $otp . rand(1, 9);
        return $otp;
    }





    public function register_user(Request $request)
    {

        $request->validate([
            'otp' => 'required',
            'imei' => 'required', 'digits:15',
            'client_token' => 'required',
            'mobile' => 'required'
        ]);
        $otp = Activecode::where('code', $request->otp)
            ->where('expired_at', '>', now())
            ->first();
        // dd($otp);
        if ($otp == null) {
            abort(403);
        } else {
            if ($request->otp == session('otp') && $request->imei == session('imei')) {
                $user = User::where('mobil_on', $request->mobile)->first();
                
                $session=$this->generateSession($request);
                if ($user) {
                    //ارسال اطلاعات contact
                    $data = [
                        'id' => $user->id,
                        'mobile' => $user->mobil_on,
                        'created_at' => $user->created_at,
                        'contact' => $user->contact(),
                        'identity' => $user->identity()
                    ];
                    Auth::loginUsingId( $user->id);
                    return json_encode($data);
                } else {
                    $user = User::create([
                        'mobil_on' => $request->mobile,
                    ]);
                    Session::create([
                        'imi' => $session,
                        'status' => 1,
                        'user_id' => $user->id
                    ]);
                    Token::create([
                        'user_id' => $user->id,
                        'token' => md5($request->client_token)
                    ]);
                    //بره صفحه پروفایل برای دریافت اطلاعات
                    $data = [
                        'session' =>  $session,
                        'new_user' => true,
                        'user_id' => $user->id
                    ];
                    Auth::loginUsingId( $user->id);
                    return json_encode($data);
                }
            }
        }
    }







    private function getAliveCode($mobile)
    {
        return !!Activecode::where('mobile', $mobile)->where('expired_at', '>', now())->first();
    }


    private function generateSession($request)
    {
        $token1_imi = md5($request->imei);
        $token1_Mobil = md5(substr($request->mobile, 4, 2) + 13);
        $token1_token2 = md5($request->client_token);
        return  md5($token1_imi . $token1_Mobil . $token1_token2);
    }
}
