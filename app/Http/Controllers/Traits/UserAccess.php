<?php


namespace App\Http\Controllers\Traits;

use App\Model\User;
use App\Model\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait UserAccess
{
    protected function UserAccess($request)
    {
       
        if ($this->CheckClientToken($request) && $this->CheckClientSession($request)) {
            return TRUE;
        }
        return FALSE;
    }



    private function CheckClientToken($request)
    {
        return !! auth()->user()->token->where('token', md5($request->client_token))->first();
    }
    private function CheckClientSession($request)
    {
        return !! auth()->user()->sessions->where('imi', md5(md5($request->mobile) . md5($request->imei)))->first();
    }
}
