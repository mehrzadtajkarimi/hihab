<?php

namespace App\Notifications\Channels;

use Ghasedak\Exceptions\ApiException;
use Ghasedak\Exceptions\HttpException;
use Illuminate\Notifications\Notification;

class GhasedakChannel
{
    public function send($notifiable, Notification $notification)
    {

        // if (!method_exists($notification, 'toGhasedakSms')) {
        //     throw new \Exception('سرویس قاصدک در دسترس نیست');
        // }

        $data = $notification->toGhasedakSms($notifiable);

        $message = $data['text'];
        $receptor = $data['mobil_on'];
        $apiKey=config('services.ghasedak.key');

        try {
            $lineNumber = "10008566";
            $api = new \Ghasedak\GhasedakApi($apiKey);
            $api->SendSimple($receptor, $message, $lineNumber);

        } catch (ApiException $e) {
            throw $e;

        } catch (HttpException $e) {
            throw $e;
        }
    }



}
