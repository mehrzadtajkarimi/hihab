<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\Channels\GhasedakChannel;
use Illuminate\Notifications\Messages\MailMessage;

class ActiveCodeNotifications extends Notification
{
    use Queueable;
    public $code;
    public $mobile_on;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($code, $mobile_on)
    {
        $this->code = $code;
        $this->mobile_on = $mobile_on;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [GhasedakChannel::class];
    }

    public function toGhasedakSms($notifiable)
    {
        return [
            'text' => "کد احراز هویت\n {$this->code}HiHab",
            'mobil_on' => "09128897603"
        ];
    }
}
