<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Activecode extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    // public function user()
    // {
    //     return $this->belongsTo(User::class);
    // }


    public function scopeVerbifyCode($query, $code, $user)
    {
        return !! $user->activecodes()->where('code', $code)->where('expired_at', '>', now())->first();
    }




    public function scopeGenerateCode($query, $user, $request)
    {
        // TODO codeExists
        if ($code = $this->getAliveCodeForUser($user)) {
            $code = $code->code;
        } else {
            do {
                $code = mt_rand(100000, 999999);
            } while ($this->checkCodeIsUnique($user, $code));

            // $user->otp()->create([
            //     'code' => $code,
            //     'expired_at' => now()->addMinutes(10),
            // ]);


            Activecode::create([
                'code' => $code,
                'user_id' => $user->id,
                'expired_at' => now()->addMinutes(10),
            ]);
        }

        return $code;
    }

    private function getAliveCodeForUser($user)
    {
        // dd(now() > $user->otp()->first());
        // if (now() > $user->otp()->first()['expired_at']) {
        //     $code = $user->otp()->first()['code'];
        // }
        // return $code;

        return $user->Activecodes()->where('expired_at', '>', now())->first();
    }

    private function checkCodeIsUnique($user, int $code)
    {
        return !! $user->activecodes()->where('code', $code)->first();
    }
}
