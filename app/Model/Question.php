<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = ['id'];
    public function options()
    {
        return $this->hasMany(Option::class);
    }
    public function package()
    {
        return $this->belongsTo(Package::class);
    }
}
