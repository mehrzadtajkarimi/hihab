<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Challenge extends Model
{
    protected $guarded = ['id'];
    public function challengepages()
    {
        return $this->hasMany(Challengepage::class);
    }
    public function package()
    {
        return $this->belongsTo(Package::class);
    }
    public function user()
    {
        return $this->belongsTo(user::class);
    }
}
