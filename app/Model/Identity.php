<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Identity extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    public function user()
        {
        return $this->hasOne(User::class);
    }
}
