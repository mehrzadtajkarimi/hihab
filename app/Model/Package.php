<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $guarded = ['id'];
    public function questions()
    {
        return $this->hasMany(Question::class);
    }
    public function challenges()
    {
        return $this->hasMany(Challenge::class);
    }
}
