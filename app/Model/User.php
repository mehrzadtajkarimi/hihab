<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    // public $otp;
    // protected $fillable = ['mobil_on'];
    protected $guarded = ['id'];

    // public function activecodes()
    // {
    //     return $this->hasMany(Activecode::class);
    // }
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }
    public function identity()
    {
        return $this->belongsTo(Identity::class);
    }
    public function contact()
    {
        return $this->hasOne(Contact::class);
    }
    public function token()
    {
        return $this->hasOne(Token::class);
    }
    public function challenge()
    {
        return $this->hasMany(Challenge::class);
    }
}
