<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $guarded = ['id'];
    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
