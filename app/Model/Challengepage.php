<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Challengepage extends Model
{
    protected $guarded = ['id'];
    public function challenge()
    {
        return $this->belongsTo(Challenge::class);
    }
}
